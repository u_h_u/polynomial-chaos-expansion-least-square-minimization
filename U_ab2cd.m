function y = U_ab2cd(x,a,b,c,d)
% Function for transformation from U([a,b]) to U([c,d])
y = (x.*(d-c)+c.*b-a.*d)./(b-a);
