function psi_alfa = eval_multivar_poly( X, alpha )
% Evaluate the multivariate polynomial with multi-indice alpha for x-values
% given in a vector X. It represents one basis "vector".
%
% Inputs:
% - alpha: degree of univariate polynomial in each input dimension. It is a
%          row vector of length equal to the number of model inputs.
% - X: input samples at which the multivariate polynomial is evaluated. It
%      is a matrix with the same number of colums as length of alpha (i.e.
%      the number of model inputs).
%
% Outputs:
% - psi_alfa: evaluation of the multivariate polyonomial at each input
%             sample. It is a column vector of length equal to the number
%             of samples.

psi_alfa = ones(size(X,1),1);   % initialize output vector psi

for k=1:size(alpha,2)        % for each model input
    psi_alfa = psi_alfa.*eval_legendre(X(:,k), alpha(k));          % Eq (4)
end

%% Comments:
% - The same equation but explicitly implementated would be:
% for i=1:size(X,1)            % for each value of X
%     for k=1:size(alpha,2)        % for each model input
%         psi_alfa(i) = psi_alfa(i)*eval_legendre(X(i,k), alpha(k));
%     end
% end
%
% - To allow inputs with different distributions, change in the product
% eval_legendre with evaluation of the correct polynomial of each input.
