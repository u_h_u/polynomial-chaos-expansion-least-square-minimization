function psi_alfas = eval_multivar_poly_basis( X, alphas )
% Evaluate the multivariate polynomial for each multi-indice alpha for 
% x-values (i.e. samples) given in a vector X. It represents the whole
% basis at which the model is projected.
%
% Inputs:
% - alphas: a vector containing multi-indice alpha. Each multi-indice alpha
%           is a row vector of length equal to the number of model inputs,
%           which represents the degree of univariate polynomial in each 
%           input dimension.
% - X: input samples at which the multivariate polynomials are evaluated.
%      It is a matrix, i.e. a vector of input samples, which are row
%      vectors of length equal to the number of model inputs.
%
% Outputs:
% - psi_alfas: evaluations of the multivariate polyonomials for each input
%              sample. It is a matrix of size equal to the number of
%              samples times the number of different multi-indice alpha.

psi_alfas = ones(size(X,1), size(alphas,1)); % initialize output matrix psi

for j=1:size(alphas,1)   % for each combination of multi-indice alpha
    psi_alfas(:,j) = eval_multivar_poly(X, alphas(j,:));           % Eq (5)
end