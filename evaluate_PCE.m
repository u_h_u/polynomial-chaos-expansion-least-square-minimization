function [mu_Y_PC_ed, var_Y_PC_ed, e_RMSE, e_LOO, X_ed, Y_M_ed] = ...
    evaluate_PCE( p, N_ed, X_va, Y_M_va, M, num_M_in, a, b )
% Generates a PCE metamodel of model M, with truncation degree p based on
% number of samples N_ed of experimental design. The model is validated on
% number of samples defined by val_ratio and N_ed.
% The function also returns the generated samples used for the model
% generation (X_ed) as well as their evaluations of the full model (Y_ed).
%
% Inputs:
% - p: maximal degree of polynomial basis (i.e. degree of truncation)
% - N_ed: number of experiments to be run, i.e. evaluated on full model
% - X_va: samples used for validation
% - Y_M_va: model evaluations of the validation samples (i.e. reference
%           responses). A vector of same size as X_va.
% - M: full model
% - num_M_in: number of inputs of the full model
% - a: lower boundaries of uniform distributions of model inputs
% - b: upper boundaries of uniform distributions of model inputs
%
% Outputs:
% - mu_Y_PC_ed: mean value of the PCE metamodel
% - var_Y_PC_ed: variance of the PCE metamodel
% - e_RMSE: Relative Mean Square Error of the generated PCE metamodel
%           validated on N_va samples
% - e_LOO: Relative Leave-One-Out Error of the generated PCE metamodel
%          sample. It is a column vector of length equal to the number of 
%          samples.
% - X_ed: generated samples of the experimental design
% - Y_ed: evaluation of the generate samples of the experimental design on
%         the full model
%

%% Evaluation of full model on experimental design, i.e. N_ed samples
X_ed = gen_samples(N_ed,num_M_in,a,b); % generation of N_ed samples

Y_M_ed = M(X_ed); % evaluation of generated samples on the full model

%% Transformation of experimental design: xi_ed_i~U([-1,1])
xi_ed = U_ab2cd(X_ed,a,b,-1,1);

%% Construction of polynomial basis and evaluation on experimental design
% Generate all possible combinations of |alpha|<=p
alphas = create_alphas(num_M_in,p); 

% Evaluation of the the experimental design on the full set of polynomial
% basis elements.
psi_ed = eval_multivar_poly_basis(xi_ed, alphas); 

%% Calculation of the polynomial coeffcients with Ordinary Least-Squares
y_alpha = pinv(psi_ed' * psi_ed) * psi_ed' * Y_M_ed;               % Eq (6)

% Calculation of PCE metamodel statistics
mu_Y_PC_ed = y_alpha(1);                                          % L5, s55
var_Y_PC_ed = sum(y_alpha(2:end).^2);                             % L5, s56

%% Evaluation of metamodel on a new set of N_va samples
xi_va = U_ab2cd(X_va,a,b,-1,1); % transformation of validation samples

% Evalutate the transformed validation set on the multivariate polynomial 
% orthonormal basis (with respect to the joind PDF of X)
psi_va = eval_multivar_poly_basis(xi_va, alphas);

% Evaluation of the validation samples on the truncated PCE metamodel
Y_PC_va = (y_alpha' * psi_va')';                                   % Eq (1)

%% Calculation of Relative Mean Square Error
% From evaluation of validation samples on the full model Y_M_va and from
% evaluation of validation samples on the PCE metamodel Y_PC_va.
e_RMSE = sum((Y_M_va - Y_PC_va).^2, 'all') / ...
         sum((Y_M_va - mean(Y_M_va)).^2, 'all');                   % Eq (7)

%% Calculation of Relative Leave-One-Out Error
% From evaluation of experimental design on the full model Y_M_ed and from
% evaluation of experimental design on the PCE metamodel Y_PC_ed.
A = psi_ed;
h = diag( A * pinv(A' * A) * A' );
Y_PC_ed = (y_alpha' * psi_ed')';
E_LOO = mean(((Y_M_ed - Y_PC_ed)./(1-h)).^2);                     % L8, s16
e_LOO = E_LOO/var(Y_M_ed, 1);                                     % L8, s15

if isinf(e_LOO)
    % Case when h == 1 & Y_M_ed != Y_PC_ed
    % => division of nonzero by zero
    % Interpertation is that error is very large
    % For easier representation compared to other cases, it can be assigned
    % a fixed number manually.
%     e_LOO = e_RMSE;   % set to e_RMSE which is the real error estimate
elseif isnan(e_LOO)
    % Case when h == 1 & Y_M_ed = Y_PC_ed
    % => division of 0 by 0
    % Interpertation is that error is very large
    % For easier representation compared to other cases, it can be assigned
    % a fixed number manually.
%     e_LOO = e_RMSE;   % set to e_RMSE which is the real error estimate
end
