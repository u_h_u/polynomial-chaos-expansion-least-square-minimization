function samples = gen_samples(N, dim, a, b, method)
% Generate N uniformly distributed samples on interval [a,b] with the given
% method.
%
% Inputs:
% - N: an integer, defining the number of random samples to be generated
% - dim: dimension of the generated samples
% - a: lower boundry of the uniform distribution
% - b: upper boundry of the unifrom distribution
% - method: a string, defining the method for generation of samples
%           The following options are allowed:
%           'LH' - Latin Hypercube
%           default/else - Monte Carlo
%
% Outputs:
% - samples: N generated random samples with uniform distribution

% By default (i.e. if method is not specified), MC generation is done
if nargin==4
    method = 'MC';
end

if strcmp(method, 'LH')
% Function for generating N Latin Hypercube samples with uniform
% distribution on interval [a,b]
    samples = U_ab2cd(lhsdesign(N,dim),0,1,a,b);
else
% Function for generating N standard Monte Carlo samples with uniform
% distribution on interval [a,b]
    samples = U_ab2cd(rand([N,dim]),0,1,a,b);    
end

