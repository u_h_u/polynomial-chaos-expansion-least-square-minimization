%% Explanation of abbreviations used in the code:
% Eq (X) - refers to equation X from the 'mini_project_description.pdf'
% LY, sZ - refers to slide Z of lecture L
% X   - refers to input samples
% xi  - refers to transformed input samples to U([-1,1])
% Y   - refers to response of a model, which can be:
%        Y_PC - turncated polynomial chaos expansion metamodel
%        Y_M - full model
% _ed - refers to experimental design, e.g. as in:
%        X_ed - input samples of the experimental design 
%        Y_PC/M_ed - response of a model on samples of experimental design
%        psi_ed - evaluation of samples of experimental design on the 
%                 multivariate polynomial orthonormal basis (with respect to the joind PDF of X)
% _va - refers to validation samples, e.g. as in:
%        X_va - validation input samples
%        Y_PC/M_va - response of a model on validation samples
% _MC - refers to Monte Carlo simulation, e.g. as in:
%        X_MC - input samples used for Monte Carlo simulation
%        Y_MC - response of a model to samples of Monte Carlo simulation
% psi - evaluation of multivariate polynomial orthonormal basis (with 
%       respect to the joind PDF of model inputs) for input samples
% N   - number of sample:
%        N_ed - number of experiments to be run, i.e. evaluated on full
%               model. Optimal value = k*(p+num_M_in)!/(p!*num_M_in!)
%               where k = [2,3]                                   % L8, s22
%        N_va - number of validation samples
% p   - maximal degree of polynomial basis (i.e. degree of truncation)
% y_alpha - vector of polynomial coefficients of PCE metamodel
%

%% Initialization
close all;
clear all;
clc;

% Initialize pseudo-random number generator with fixed seed for 
% repeatability
rng(100);   

% Set number of variable precision used	
digits(60);

% Load the model with description of its probabilistic inputs, i.e. their
% uniform distribution boundaries (function M and variables num_M_in, a, b)
[M, num_M_in, a, b, mu_Y_ref, var_Y_ref] = model();

% Define number of samples used for Monte Carlo simulation
N_MC = 10^6;
% Get reference mean, variance and model response from MC for validation.
[mu_Y_MC, var_Y_MC, Y_MC, X_MC] = monte_carlo(N_MC, M, num_M_in, a, b);
% Define number of samples used for validation (evaluation of all MC 
% samples takes too much memory for calculation of validation)
N_va = 10^5;
X_MC = X_MC(1:N_va,:);
Y_MC = Y_MC(1:N_va,:);


% Number of samples for PCE:
% if k<1 => model overfits
% optimal number of samples is for k=2-3
N_PCE = @(M,p,k)ceil((factorial(M+p)./(factorial(M).*factorial(p))).*k);

% Define all truncation degrees used for generation of different PCE
% metamodels.
p = [2,3,5,7,10];

% Define all number of samples of experimental design used for generation
% of PCE metamodels.
% OLD:
% N_ed = [40 150 350 1050 2000 3000 5000 7000 10000];
% NEW:
N_ed = [40 90 150 350 1050 2500 10000];

% Initialize mean and variance evaluations of PCE metamodels
mu_Y_PC_ed  = zeros(length(p),length(N_ed));
var_Y_PC_ed = zeros(length(p),length(N_ed));

% Initialize error estimations of PCE metamodels
e_RMSE = zeros(length(p),length(N_ed));
e_LOO  = zeros(length(p),length(N_ed));

% Initialize storing of generated samples and their evaluations of the 
% experimental design.
X_ed = cell(length(p),length(N_ed));
Y_M_ed = cell(length(p),length(N_ed));

%% Evaluate PCE for different truncation degrees and samples of experimental design
for i = 1:length(p)         % for each truncation degree 
    for j = 1:length(N_ed)      % for each experimental design
        [mu_Y_PC_ed(i,j), var_Y_PC_ed(i,j), e_RMSE(i,j), e_LOO(i,j), ...
            X_ed{i,j}, Y_M_ed{i,j}] = ...
        evaluate_PCE(p(i), N_ed(j), X_MC, Y_MC, M, num_M_in, a, b);
    end
end

%% Plot our results
model_method = 'our';     % specify model method for different nameing of
                            % stored plots
plot_results(p, N_ed, mu_Y_PC_ed, var_Y_PC_ed, e_RMSE, ...
    e_LOO, mu_Y_ref, var_Y_ref, model_method)