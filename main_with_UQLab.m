%% *NOTE: For this to work, it needs to be placed in the folder with UQLab!
% If you wish to run it without UQLab validation, run: 'main.m'

%% Explanation of abbreviations used in the code:
% Eq (X) - refers to equation X from the 'mini_project_description.pdf'
% LY, sZ - refers to slide Z of lecture L
% X   - refers to input samples
% xi  - refers to transformed input samples to U([-1,1])
% Y   - refers to response of a model, which can be:
%        Y_PC - turncated polynomial chaos expansion metamodel
%        Y_M - full model
% _ed - refers to experimental design, e.g. as in:
%        X_ed - input samples of the experimental design 
%        Y_PC/M_ed - response of a model on samples of experimental design
%        psi_ed - evaluation of samples of experimental design on the 
%                 multivariate polynomial orthonormal basis (with respect to the joind PDF of X)
% _va - refers to validation samples, e.g. as in:
%        X_va - validation input samples
%        Y_PC/M_va - response of a model on validation samples
% _MC - refers to Monte Carlo simulation, e.g. as in:
%        X_MC - input samples used for Monte Carlo simulation
%        Y_MC - response of a model to samples of Monte Carlo simulation
% psi - evaluation of multivariate polynomial orthonormal basis (with 
%       respect to the joind PDF of model inputs) for input samples
% N   - number of sample:
%        N_ed - number of experiments to be run, i.e. evaluated on full
%               model. Optimal value = k*(p+num_M_in)!/(p!*num_M_in!)
%               where k = [2,3]                                   % L8, s22
%        N_va - number of validation samples
% p   - maximal degree of polynomial basis (i.e. degree of truncation)
% y_alpha - vector of polynomial coefficients of PCE metamodel
%

%% Initialization
close all;
clear all;
clc;

% Program does the same thing as main.m but also using UQLab. This is meant
% as validation of our implementation of PCE.

% Initialize pseudo-random number generator with fixed seed for 
% repeatability
rng(100);   

% Set number of variable precision used
digits(100);

% Load the model with description of its probabilistic inputs, i.e. their
% uniform distribution boundaries (function M and variables num_M_in, a, b)
[M, num_M_in, a, b, mu_Y_ref, var_Y_ref] = model();
% Define number of samples used for Monte Carlo simulation
N_MC = 10^6;
% Get reference mean, variance and model response from MC for validation.
[mu_Y_MC, var_Y_MC, Y_MC, X_MC] = monte_carlo(N_MC, M, num_M_in, a, b);
% Define number of samples used for validation (evaluation of all MC 
% samples takes too much memory for calculation of validation)
N_va = 10^5;
X_MC = X_MC(1:N_va,:);
Y_MC = Y_MC(1:N_va,:); 

%%
% Define all truncation degrees used for generation of different PCE metamodels.
p = [2,3,5,7,10];

% Define all number of samples of experimental design used for generation
% of PCE metamodels via UQ_Lab.

N_ed = [40 90 150 350 1050 2500 10000];

% Initialize mean and variance evaluations of PCE metamodel with UQ_Lab
mu_Y_PC_ed  = zeros(length(p),length(N_ed));
var_Y_PC_ed = zeros(length(p),length(N_ed));
mu_Y_PC_ed_our  = zeros(length(p),length(N_ed));
var_Y_PC_ed_our = zeros(length(p),length(N_ed));

% Initialize error estimations of PCE metamodels with UQ_Lab
e_RMSE = zeros(length(p),length(N_ed));
e_LOO  = zeros(length(p),length(N_ed));
e_RMSE_our = zeros(length(p),length(N_ed));
e_LOO_our  = zeros(length(p),length(N_ed));

% Initialize storing of generated samples and their evaluations of the 
% experimental design.
X_ed = cell(length(p),length(N_ed));
Y_M_ed = cell(length(p),length(N_ed));

%% STARTUP THE UQLAB FRAMEWORK
uqlab;

%% Evaluate UQ Lab PCE for different truncation degrees and samples of experimental design
for i = 1:length(p)         % for each truncation degree 
    for j = 1:length(N_ed)      % for each experimental design
        [mu_Y_PC_ed_our(i,j), var_Y_PC_ed_our(i,j), e_RMSE_our(i,j), ...
            e_LOO_our(i,j), X_ed{i,j}, Y_M_ed{i,j}] = ...
        evaluate_PCE(p(i), N_ed(j), X_MC, Y_MC, M, num_M_in, a, b);
    
%%  DEFINE THE INPUT VECTOR
% Create an input vector based on the distributions in mini-project report

% Load in kN
Input.Marginals(1).Name = 'P';
Input.Marginals(1).Type = 'Uniform';
Input.Marginals(1).Parameters = [0.98 3.92];

% Beam lengt in mm
Input.Marginals(2).Name = 'L';
Input.Marginals(2).Type = 'Uniform';
Input.Marginals(2).Parameters = [2990 3010];

% Elastic Modulus kN/mm^2
Input.Marginals(3).Name = 'E';
Input.Marginals(3).Type = 'Uniform';
Input.Marginals(3).Parameters = [194.35 217.65];

% Moment of inerata mm^4
Input.Marginals(4).Name = 'I';
Input.Marginals(4).Type = 'Uniform';
Input.Marginals(4).Parameters = [363678.5 363921.5];

myInput=uq_createInput(Input);    

%% CREATE THE PCE METAMODEL
%  Configure the PCE according to the Tutorial requirements

% Use UQLab metamodelling facility 
Metaopts.Type = 'uq_metamodel'; % [DO NOT MODIFY!] 
% Run a Polynomial Chaos Expansion 
Metaopts.MetaType = 'PCE';  % [DO NOT MODIFY!] 
% Use probabilistic input model specified earlier. This will be used by
% UQLab to calculate the necessary isoprobabilistic transforms from 
% Lognormal to Gaussian distributions in the input. 
Metaopts.Input = myInput;  % [DO NOT MODIFY!]

%% PARAMETERS THAT CAN BE MODIFIED BY THE STUDENTS
% Select the least-square minimization method: 'OLS' (Ordinary Least
% Squares) or 'LARS' (Least Angle Regression) for sparse PCE 
Metaopts.Method = 'OLS' ; 
% Select the q-norm truncation value (between 0.5 and 1)
Metaopts.TruncOptions.qNorm = 1 ;
% Select data for experimental design
Metaopts.ExpDesign.X = X_ed{i,j};
Metaopts.ExpDesign.Y = Y_M_ed{i,j};

% Select the maximum polynomial degree.
Metaopts.Degree =  p(i); %

% create the metamodel and add it to UQLab
myPCE = uq_createModel(Metaopts); % [DO NOT MODIFY!]

%% RETRIEVE RESULTS
% get the PCE coefficients
results.coeffs = myPCE.PCE.Coefficients ;

% get the approximate mean value based on the PCE coefficients 
mu_Y_PC_ed(i,j) = results.coeffs(1);

% get the approximate variance value based on the PCE coefficients 
var_Y_PC_ed(i,j) = sum(results.coeffs(2:end).^2);

% get the LOO Error
e_LOO(i,j) = myPCE.Error.LOO;

%% Calculation of Relative Mean Square Error

% From evaluation of validation samples on the full model Y_M_va and from
% evaluation of validation samples on the PCE metamodel Y_PC_va.
e_RMSE(i,j) = sum((Y_MC - uq_evalModel(myPCE,X_MC)).^2, 'all') / ...
         sum((Y_MC - mean(Y_MC)).^2, 'all');                   % Eq (7)

    end
end

%% Plot our results
model_method = 'our';     % specify model method for different nameing of
                            % stored plots
plot_results(p, N_ed, mu_Y_PC_ed_our, var_Y_PC_ed_our, e_RMSE_our, ...
    e_LOO_our, mu_Y_ref, var_Y_ref, model_method)

%% Plot UQLab results
model_method = 'UQLab';     % specify model method for different nameing of
                            % stored plots
plot_results(p, N_ed, mu_Y_PC_ed, var_Y_PC_ed, e_RMSE, ...
    e_LOO, mu_Y_ref, var_Y_ref, model_method)

%% Plot comparison of UQLab and our implementation
e_mu = abs(mu_Y_PC_ed_our./mu_Y_PC_ed-1);
e_var = abs(var_Y_PC_ed_our./var_Y_PC_ed-1);
e_rmse = abs(e_RMSE_our./e_RMSE-1);
e_loo = abs(e_LOO_our./e_LOO-1);

e_all = {e_mu, e_var, e_rmse, e_loo};
e_all_str = {'e_{mu}','e_{var}','e_{rmse}','e_{loo}'};

% Evaluate on the following combinations
com = [[ 1 1 0 0 0 ];
       [ 1 1 0 0 0 ];
       [ 0 0 0 0 0 ];
       [ 0 0 1 0 0 ];
       [ 0 0 0 0 0 ];
       [ 0 0 0 0 0 ];
       [ 0 0 0 1 1 ]];
% has to be of dimensions length(p)*length(N_ed)    
com = logical(com');
dpx = ones(1, length(com(com)));
xlabs = cell([length(dpx),1]);
leg = cell([length(e_all),1]);
c = 1;
for i=1:size(com,1)
    for j=1:size(com,2)
        if com(i,j)
            xlabs{c} = ['(', num2str(N_ed(j)), ', ', num2str(p(i)) ,')'];
            dpx(c) = c;
            c = c + 1;
        end
    end
end

fval = figure('Name','Validation: our implementation vs UQLab');
for j = 1:length(e_all)         % for each truncation degree 
    dpy = cell2mat(e_all(j));
    dpy = dpy(com);
    semilogy(dpx, dpy);
    leg{j} = e_all_str{j};
    hold on;
end
title('Our implementation vs UQLab');
xlabel('(number of samples, truncation degree)');
xticklabels(xlabs);
legend(leg);
ylabel('|par_{our}/par_{UQLab} - 1|')
hold off;

saveas(fval,['figs/our_vs_UQLab','.jpg'])