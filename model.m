function [M, num_M_in, a, b, mu_Y_ref, var_Y_ref] = model()
% Model of maximal deflection of a simply supported hollow beam
% v(L/2) = v_max = (P*L^3)/(48*E*I)
% The vector of the inputs is the following:
% X(1) = P, X(2) = L, X(3) = E, X(4) = I
%

M = @(X) (X(:,1).*X(:,2).^3)./(48.*X(:,3).*X(:,4));
num_M_in = 4;  % number of model inputs

%% Probabilistic input models
% Distributions are assumed to be uniform: X_i~U([a_i,b_i])
% Variables a and b contain the lower and upper boundaries of each input
% parameter, respectively. Each is of size 1xM.
a = [0.98, 2990, 194.35, 363678.5]; % Lower boundaries of distributions a_i
b = [3.92, 3010, 217.65, 363921.5]; % Upper boundaries of distributions b_i

%% Reference values for mean and variance (from analytical solution)
mu_Y_ref  = 18.40887208642694421073585418792304;
var_Y_ref = 41.08474577397387547285359444229818;
