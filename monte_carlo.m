function [mu_Y_MC, var_Y_MC, Y_MC, X_MC] = ...
    monte_carlo( N_MC, M, num_M_in, a, b )
%% Monte Carlo simulation of the full model
% Get reference values of mean and variance for validation and reference
% model response.
%
% Inputs:
% - N_MC: number of samples used for MC
% - M: model for MC simulation
% - num_M_in: number of inputs of the full model
% - a: lower boundaries of uniform distributions of model inputs
% - b: upper boundaries of uniform distributions of model inputs
%
% Outputs:
% - mu_Y_MC: reference mean calculated from MC
% - var_Y_MC: reference variance calculated from MC
% - X_MC: input samples used for MC
% - Y_M_va: model responses to the input samples of MC (i.e. reference
%           responses). A vector of same size as X_MC.
%

X_MC = gen_samples(N_MC,num_M_in,a,b); % generation of samples for MC

Y_MC = M(X_MC); % evaluation of the model on MC samples

% Calculate model statistics from Monte Carlo for reference
mu_Y_MC = mean(Y_MC);   % reference mean value
var_Y_MC = var(Y_MC);   % reference variance