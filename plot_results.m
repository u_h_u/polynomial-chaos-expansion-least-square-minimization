function [] = plot_results(p, N_ed, mu_Y_PC_ed, var_Y_PC_ed, e_RMSE, ...
    e_LOO, mu_Y_ref, var_Y_ref, model_method)
%% Plot results
% Separate file just for plotting the results

JOINT = false;  % set to true for subplot of images
               % set to false to display each individual figure (creates
               % folder named 'figures' and stores them inside)

%% ************************************************************************
% Convergence of mean and variance for fix number of samples of
% experimental design and changing truncation degree.

% Plot the following combinations of p and N_ed:
com_N = [[ 1 1 0 0 0 ];
         [ 1 1 0 0 0 ];
         [ 1 1 1 0 0 ];
         [ 0 1 1 1 0 ];
         [ 0 1 1 1 1 ];
         [ 0 0 1 1 1 ];
         [ 0 0 0 1 1 ]];
% has to be of dimensions length(p)*length(N_ed)    
com_N = logical(com_N');

leg = cell(length(N_ed),1);

if JOINT
    f1 = figure('Name','Convergence of mean, standard deviation, e_RMSE and e_LOO dependent on p for fixed N_ed');
    subplot(2,2,1)
else
    f1 = figure('Name','Convergence of mean depending on truncation degree');
end

for j = 1:length(N_ed)         % for each truncation degree 
    dpx = p(com_N(:,j));
    dpy = abs(mu_Y_PC_ed(:,j)/mu_Y_ref-1);
    dpy = dpy(com_N(:,j));
    semilogy(dpx, dpy);
    leg{j} = num2str(N_ed(j));
    hold on;
end
title('Convergence of mean depending on truncation degree');
legend(leg);
axis([min(p),max(p),...
    double(min(min(abs(mu_Y_PC_ed(com_N)/mu_Y_ref-1)))/2),...
    double(max(max(abs(mu_Y_PC_ed(com_N)/mu_Y_ref-1)))*2)])
xlabel('Truncation degree') 
ylabel('|\mu/\mu_{ref} - 1|')
hold off;

if JOINT
    subplot(2,2,2)
else
    f2 = figure('Name','Convergence of standard deviation depending on truncation degree');
end

for j = 1:length(N_ed)         % for each truncation degree 
    dpx = p(com_N(:,j));
    dpy = abs(sqrt(var_Y_PC_ed(:,j))/sqrt(var_Y_ref)-1);
    dpy = dpy(com_N(:,j));
    semilogy(dpx, dpy);
    leg{j} = num2str(N_ed(j));
    hold on;
end
title('Convergence of standard deviation depending on truncation degree');
legend(leg)
axis([min(p),max(p),...
    double(min(min(abs(sqrt(var_Y_PC_ed(com_N))./sqrt(var_Y_ref)-1)))/2),...
    double(max(max(abs(sqrt(var_Y_PC_ed(com_N))./sqrt(var_Y_ref)-1)))*2)])
xlabel('Truncation degree') 
ylabel('|\sigma/\sigma_{ref} - 1|')
hold off;

if JOINT
    subplot(2,2,3)
else
    f3 = figure('Name','Convergence of e_{RMSE} depending on truncation degree');
end

for j = 1:length(N_ed)         % for each truncation degree 
    dpx = p(com_N(:,j));
    dpy = e_RMSE(:,j);
    dpy = dpy(com_N(:,j));
    semilogy(dpx, dpy);
    leg{j} = num2str(N_ed(j));
    hold on;
end
title('Convergence of e_{RMSE} depending on truncation degree');
legend(leg)
axis([min(p),max(p),double(min(min(e_RMSE(com_N)))/10),double(max(max(e_RMSE(com_N)))*10)])
xlabel('Truncation degree') 
ylabel('e_{RMSE}')
hold off;

if JOINT
    subplot(2,2,4)
else
    f4 = figure('Name','Convergence of e_{LOO} depending on truncation degree');
end

for j = 1:length(N_ed)         % for each truncation degree 
    dpx = p(com_N(:,j));
    dpy = e_LOO(:,j);
    dpy = dpy(com_N(:,j));
    semilogy(dpx, dpy);
    leg{j} = num2str(N_ed(j));
    hold on;
end
title('Convergence of e_{LOO} depending on truncation degree');
legend(leg)
axis([min(p),max(p),double(min(min(e_LOO(com_N)))/10),double(max(max(e_LOO(com_N)))*10)])
xlabel('Truncation degree') 
ylabel('e_{LOO}')
hold off;

%% ************************************************************************
% Convergence of mean and variance for fix truncation degree and changing 
% number of samples of experimental design.

% Plot the following combinations of p and N_ed:
com_p = [[ 1 1 0 0 0 ];
         [ 1 1 0 0 0 ];
         [ 1 1 1 0 0 ];
         [ 1 1 1 1 0 ];
         [ 1 1 1 1 1 ];
         [ 1 1 1 1 1 ];
         [ 1 1 1 1 1 ]];
 % has to be of dimensions length(p)*length(N_ed)
com_p = logical(com_p');

leg = cell(length(p),1);

if JOINT
    f2 = figure('Name','Convergence of mean, standard deviation, e_RMSE and e_LOO dependent on N_ed for fixed p');
    subplot(2,2,1)
else
    f5 = figure('Name','Convergence of mean depending on number of samples');
end

for i = 1:length(p)         % for each truncation degree
    dpx = N_ed(com_p(i,:));
    dpy = abs(mu_Y_PC_ed(i,:)/mu_Y_ref-1);
    dpy = dpy(com_p(i,:));
    loglog(dpx, dpy);
    leg{i} = num2str(p(i));
    hold on;
end
title('Convergence of mean depending on number of samples');
legend(leg);
axis([min(N_ed),max(N_ed),...
    double(min(min(abs(mu_Y_PC_ed(com_p)/mu_Y_ref-1)))/2),...
    double(max(max(abs(mu_Y_PC_ed(com_p)/mu_Y_ref-1)))*2)])
xlabel('Number of samples') 
ylabel('|\mu/\mu_{ref} - 1|')
hold off;

if JOINT
    subplot(2,2,2)
else
    f6 = figure('Name','Convergence of standard deviation depending on number of samples');
end

for i = 1:length(p)         % for each truncation degree
    dpx = N_ed(com_p(i,:));
    dpy = abs(sqrt(var_Y_PC_ed(i,:))/sqrt(var_Y_ref)-1);
    dpy = dpy(com_p(i,:));
    loglog(dpx, dpy);
    leg{i} = num2str(p(i));
    hold on;
end
title('Convergence of standard deviation depending on number of samples');
legend(leg)
axis([min(N_ed),max(N_ed),...
    double(min(min(abs(sqrt(var_Y_PC_ed(com_p))./sqrt(var_Y_ref)-1)))/2),...
    double(max(max(abs(sqrt(var_Y_PC_ed(com_p))./sqrt(var_Y_ref)-1)))*2)])
xlabel('Number of samples') 
ylabel('|\sigma/\sigma_{ref} - 1|')
hold off;

if JOINT
    subplot(2,2,3)
else
    f7 = figure('Name','Convergence of e_{RMSE} depending on truncation degree');
end

for i = 1:length(p)         % for each truncation degree 
    dpx = N_ed(com_p(i,:));
    dpy = e_RMSE(i,:);
    dpy = dpy(com_p(i,:));
    loglog(dpx, dpy);
    leg{i} = num2str(p(i));
    hold on;
end
title('Convergence of e_{RMSE} depending on number of samples');
legend(leg)
axis([min(N_ed),max(N_ed),double(min(min(e_RMSE(com_p)))/2),double(max(max(e_RMSE(com_p)))*2)])
xlabel('Number of samples') 
ylabel('e_{RMSE}')
hold off;

if JOINT
    subplot(2,2,4)
else
    f8 = figure('Name','Convergence of e_{LOO} depending on truncation degree');
end

for i = 1:length(p)         % for each truncation degree 
    dpx = N_ed(com_p(i,:));
    dpy = e_LOO(i,:);
    dpy = dpy(com_p(i,:));
    loglog(dpx, dpy);
    leg{i} = num2str(p(i));
    hold on;
end
title('Convergence of e_{LOO} depending on number of samples');
legend(leg)
axis([min(N_ed),max(N_ed),double(min(min(e_LOO(com_p)))/2),double(max(max(e_LOO(com_p)))*2)])
xlabel('Number of samples') 
ylabel('e_{LOO}')
hold off;

%% Save individual figures and close them
if ~JOINT
    fs = [f1, f2, f3, f4, f5, f6, f7, f8];
    if ~exist('figs', 'dir')
       mkdir('figs')
    end
    for i=1:8
        saveas(fs(i),['figs/figure_', num2str(i), '_', model_method, '.jpg'])
    end
    close all;
end